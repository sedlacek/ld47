package com.sedlacek.ld47.gui;

import com.sedlacek.ld47.graphics.ImageLoader;
import com.sedlacek.ld47.main.Config;
import com.sedlacek.ld47.main.Game;
import com.sedlacek.ld47.main.KeyManager;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Menu extends GUI{

    public static BufferedImage menuArt = ImageLoader.loadNS("/menuArt.png");

    public static final String[] TEXTS = new String[]{
            "to begin your journey...",
            "to escape the loop...",
            "to start a new game..."
    };

    //private Random r = new Random();
    private long lastTime;
    private long interval = 20;
    private int index = 0;
    private int transpar = 255;
    private boolean decr = true;

    public Menu(){
        lastTime = System.currentTimeMillis();
    }

    @Override
    public void update() {
        super.update();
        if(System.currentTimeMillis()-lastTime >= interval){
            if(decr) {
                if (transpar <= 10) {
                    index++;
                    if (index >= TEXTS.length) {
                        index = 0;
                    }
                    decr = !decr;
                } else {
                    transpar -= 3;
                }
            }else{
                if (transpar >= 240) {
                    decr = !decr;
                } else {
                    transpar += 3;
                }
            }
            lastTime = System.currentTimeMillis();
        }
        if(KeyManager.anyKeyPressed){
            if(Game.getKeyManager().keys[KeyManager.T]){
                Tutorial.disableTuts = !Tutorial.disableTuts;
                Game.getKeyManager().keys[KeyManager.T] = false;
                KeyManager.anyKeyPressed = false;
            }else {
                Game.state = Game.State.GAME;
                Game.simulation.tutorial.start();
            }
        }
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(menuArt, 0, 0, Config.WIDTH, Config.HEIGHT, null);
        g.setColor(new Color(0,0,0,70));
        g.fillRect(0,0, Config.WIDTH, Config.HEIGHT);
        g.setColor(Color.WHITE);
        g.setFont(new Font("DorFont03", Font.BOLD, 100));
        g.drawString("Solar Edge", Config.WIDTH/2-g.getFontMetrics().stringWidth("Solar Edge")/2, 200);
        g.setColor(new Color(255, 255, 255, transpar));
        g.setFont(new Font("DorFont03", Font.PLAIN, 32));
        String startText = "Press [Enter] "+TEXTS[index];
        g.drawString(startText, Config.WIDTH/2-g.getFontMetrics().stringWidth(startText)/2, Config.HEIGHT/2);

        g.setColor(new Color(255,255,255,80));
        g.setFont(new Font("DorFont03", Font.PLAIN, 16));
        g.drawString("Made by Marek Sedlacek for Ludum Dare 47 in 48 hrs", 10, Config.HEIGHT-40);
        g.setColor(new Color(255,255,255,70));
        g.setFont(new Font("DorFont03", Font.PLAIN, 16));
        if(Tutorial.disableTuts){
            g.drawString("Press [T] to enable tips", Config.WIDTH - 60 - g.getFontMetrics().stringWidth("[T] to enable tips"),
                    Config.HEIGHT - 40);
        }else {
            g.drawString("Press [T] to disable tips", Config.WIDTH - 60 - g.getFontMetrics().stringWidth("[T] to disable tips"),
                    Config.HEIGHT - 40);
        }
    }
}
