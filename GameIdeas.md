# Theme: Stuck in a loop
    
## Circular map = Loop!

**Stuck** - by gravitational field - has to get enough resources to leave it!

Space RTS where you occupy small planets each with different challenges and resources.  
You have some goal, but to achieve it you have to achieve smaller goals on all planets.  
You are forced (stuck in a loop) to change planets after some time/event needing to get all needed to the next planet, otherwise you'll have to wait for another rotation.  

Maybe planet change would be possible only when aligned and would have to be explicitly done by the player.  

Convoy could be interrupted by pirates or other events could happen.  

## Gameplay

* You have (maybe even pick) a ship with set resources
    * Escape/travel pods - Used to travel from orbit down the the planet and back up and to search through near space (merchants...).
    * Reactor - For fuel, there could be some upgrades.
    * Heating/Cooling system - for when you're too close or far away from sun.
    * Storage facility - To store resources
* When you arrive at a planet, the capsule occupies a random square and changes it to the landsite (possibly crash site and then needs to be repaired). 
* You have certain amount of people which are needed to do some work. You can make more by cloning/lab synthesis, but it isn't cheap. Or you can get some by saving people's ships - chance of it to backfire.
* Every planet has different set of tiles, where there is always some fuel (what element/molecule?).

### Resources

* People - Can be synthesised or hired.
* Fuel & heating/cooling - Natural gases (Empty tile)
* Air - Oxygen (Water tile)
* Building material - Metals (Metal tile)
* Trading/Currency - Uranium
* Hyperspace fuel - Uranium
* Food - Either synthesised or greenhouses (Bio tile)

### Stations

* Travel pod - Has certain capacity

* Human synthesis - Replicator - On board (has to be built?)
* Natural gases - Drilling rig
* Oxygen - Water
* Metals - Scrap/metal-enriched rocks
* Food - Bio tile
* Uranium - Collected by search crew

### Style

* Pixel art low res (16x16)
* 3rd person view
* Just clicking
* Space and planet views

### What to emphasise

* GUI
* Art (animations of buildings, otherwise the world will look dead)
* Have a goal and possibly endless run (maybe even with highest score pulled from the internet)
* Have a way to fail (sun explodes?)
* Tutorial
* Add yellow borders when sun is close and blinking text

### Possibilities

* Randomly generated worlds.
* Score would be gained by skipping to other galaxy.
* The longer you wait the stronger the gravitational field gets?
* Have the option to wait in space and just explore.
* Multiple game difficulties 
* Add event timeline (next expansion)
* Transition animations
* Add fly duration - when resources are also used up
