package com.sedlacek.ld47.world;

import com.sedlacek.ld47.graphics.ImageLoader;
import com.sedlacek.ld47.gui.Button;
import com.sedlacek.ld47.main.Config;
import com.sedlacek.ld47.main.Game;

import java.awt.*;

public class O2Generator extends Station{

    public static final int PRICE = 10*6;

    public O2Generator(Tile tile){
        this.name = "Oxygen generator";
        this.description = new String[]{
                "Used to extract",
                "O2 from water.",
        };
        try {
            actionButton = new Button("Assign", 0, 0, 130, 25, Config.yellow.darker().darker(),
                    Color.BLACK, this.getClass().getDeclaredMethod("action"), this);
            actionButton.disableOnPause = true;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        this.image = ImageLoader.loadNS("/o2gen.png");
        this.tile = tile;
        this.w = 10*MULTIPLIER;
        this.h = 13*MULTIPLIER;
        this.x = tile.getX()+tile.getW()/2-w/2;
        this.y = tile.getY()+tile.getH()/2-h+3*MULTIPLIER;
        this.price = PRICE;
        this.crewSize = 2;
        this.gain = 14;
    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public void fixedUpdate(){
        if(this.active) {
            Game.simulation.o2 += gain;
        }
    }

    @Override
    public void action() {
        super.action();
    }
}
