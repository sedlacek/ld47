package com.sedlacek.ld47.world;

import com.sedlacek.ld47.graphics.ImageLoader;
import com.sedlacek.ld47.gui.Button;
import com.sedlacek.ld47.main.Config;
import com.sedlacek.ld47.main.Game;

import java.awt.*;

public class Greenhouse extends Station{

    public static final int PRICE = 20*6;
    private long ticks = 0;

    public Greenhouse(Tile tile){
        this.name = "Bio Greenhouse";
        this.description = new String[]{
                "Used to grow",
                "plants for",
                "nurture.",
        };
        try {
            actionButton = new Button("Assign", 0, 0, 130, 25, Config.yellow.darker().darker(),
                    Color.BLACK, this.getClass().getDeclaredMethod("action"), this);
            actionButton.disableOnPause = true;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        this.image = ImageLoader.loadNS("/greenhouse.png");
        this.tile = tile;
        this.w = 16*MULTIPLIER;
        this.h = 17*MULTIPLIER;
        this.x = tile.getX()+tile.getW()/2-w/2;
        this.y = tile.getY()+tile.getH()/2-h+6*MULTIPLIER;
        this.price = PRICE;
        this.gain = 1;
        this.crewSize = 4;
    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public void fixedUpdate(){
        if(this.active) {
            ticks++;
            if (ticks >= 3) {
                Game.simulation.food += gain;
                ticks = 0;
            }
        }
    }

    @Override
    public void action() {
        super.action();
    }
}
