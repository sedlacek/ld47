package com.sedlacek.ld47.gui;

import com.sedlacek.ld47.main.Config;
import com.sedlacek.ld47.main.Game;
import com.sedlacek.ld47.main.GameObject;
import com.sedlacek.ld47.objects.Planet;
import com.sedlacek.ld47.player.Simulation;
import com.sedlacek.ld47.world.Station;

import java.awt.*;

public class StationInfo extends GameObject {

    private Station s;
    private Font font = new Font("DorFont03", Font.PLAIN, 16);
    private Font titleFont = new Font("DorFont03", Font.BOLD, 16);
    private Color backgroundC = Config.yellow;
    private Color fontColor = Color.BLACK;
    private Simulation sim;

    private int xoffset = 5;
    private int yoffset = 25;
    private int space = 15;

    public StationInfo(Simulation sim, Station s){
        this.s = s;
        this.w = 150;
        this.h = s.getDescription().length*space+70;
        if(s.getActionButton() != null){
            h += 35;
        }
        this.x = s.getX()+s.getW()+20;
        this.y = s.getY();
        this.sim = sim;
        if(y + h > Config.HEIGHT - 35){
            this.y = s.getY()-h+s.getH();
        }
        if(x + w > Config.WIDTH - 20){
            this.x = s.getX()-w-15;
        }
    }

    @Override
    public void update() {
        if(s.getActionButton() != null){
            s.getActionButton().setX(x+xoffset);
            s.getActionButton().setY(y+h-30);
            s.getActionButton().update();
        }
    }

    @Override
    public void render(Graphics g) {
        g.setColor(backgroundC);
        g.fillRoundRect(x, y, w, h, 20, 20);
        g.setFont(titleFont);
        g.setColor(fontColor);
        g.drawString(s.getName(), x+xoffset, y+yoffset);
        this.w = g.getFontMetrics().stringWidth(s.getName())+xoffset*2;
        if(s.isActive()) {
            g.setColor(new Color(0x319700));
            g.drawString("Active", x+xoffset, y+yoffset+20);
        }else{
            g.setColor(new Color(0xBB0000));
            g.drawString("Idle", x+xoffset, y+yoffset+20);
        }
        g.setFont(font);
        g.setColor(fontColor);
        for(int i = 0; i < s.getDescription().length; i++) {
            g.drawString(s.getDescription()[i], x+xoffset, y+yoffset+space*i+30+20);
        }
        if(s.getActionButton() != null){
            s.getActionButton().render(g);
        }
    }

    @Override
    public void clicked() {

    }
}
