package com.sedlacek.ld47.objects;

import com.sedlacek.ld47.graphics.ImageLoader;

public class WaterPlanet extends Planet {

    public WaterPlanet(int distance){
        super(distance);
        this.img = ImageLoader.loadNS("/planet4.png");
        this.id = 3;
        this.description = new String[]{
                "Most of this",
                "planet is water",
                "containing lots of",
                "various elements.",
        };
        this.bioRepr = 0.1f;
        this.metalRepr = 0.1f;
        this.emptyRepr = 0.1f;
        this.waterRepr = 0.7f;
        super.generateMap();
        this.uraniumChance = 0.68f;
        this.peopleChance = 0.24f;
        this.metalChance = 0.21f;
        this.deathChance = 0.18f;
    }
}
