package com.sedlacek.ld47.objects;

import com.sedlacek.ld47.main.GameObject;
import com.sedlacek.ld47.player.Simulation;

import java.awt.image.BufferedImage;

public abstract class Ship extends GameObject {

    protected BufferedImage solarIcon;
    protected int multiplier = 1;
    protected int planetIndex;
    protected float angle;
    protected float speed;
    protected int distance;
    protected Simulation sim;
    protected int xc, yc;

    public void setPlanetIndex(int planetIndex) {
        this.planetIndex = planetIndex;
    }

    public int getPlanetIndex() {
        return planetIndex;
    }
}
