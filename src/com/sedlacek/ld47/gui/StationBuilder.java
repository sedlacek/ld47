package com.sedlacek.ld47.gui;

import com.sedlacek.ld47.main.Game;
import com.sedlacek.ld47.main.KeyManager;

import java.awt.*;
import java.awt.image.BufferedImage;

public class StationBuilder {

    private BufferedImage icon;
    private int multiplier;
    private int xoffs = 30;
    private int yoffs = 30;

    public StationBuilder(BufferedImage icon, int multiplier){
        this.icon = icon;
        this.multiplier = multiplier;
    }

    public void update(){
        if(Game.getKeyManager().keys[KeyManager.ESC]){
            Game.simulation.planetView.builderGUI.endStationBuilder();
            Game.getKeyManager().keys[KeyManager.ESC] = false;
        }
        if(Game.getMouseManager().RClicked){
            Game.simulation.planetView.builderGUI.endStationBuilder();
            Game.getMouseManager().RClicked = false;
        }
    }

    public void render(Graphics g){
        g.drawImage(icon, Game.getMouseManager().MX+xoffs-icon.getWidth()*multiplier,
                Game.getMouseManager().MY+yoffs-icon.getHeight()*multiplier,
                icon.getWidth()*multiplier, icon.getHeight()*multiplier, null);
    }
}
