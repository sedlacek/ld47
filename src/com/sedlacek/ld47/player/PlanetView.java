package com.sedlacek.ld47.player;

import com.sedlacek.ld47.graphics.ImageLoader;
import com.sedlacek.ld47.gui.BuilderGUI;
import com.sedlacek.ld47.gui.PlanetOverlay;
import com.sedlacek.ld47.main.Config;
import com.sedlacek.ld47.objects.Planet;

import java.awt.*;
import java.awt.image.BufferedImage;

public class PlanetView extends View{

    private Simulation sim;
    public static final BufferedImage backImg = ImageLoader.loadNS("/background.png");
    public PlanetOverlay overlay;
    public BuilderGUI builderGUI;

    public PlanetView(Simulation sim){
        this.sim = sim;
        this.overlay = new PlanetOverlay(sim);
        this.builderGUI = new BuilderGUI(sim);
    }

    @Override
    public void update() {
        super.update();
        this.builderGUI.update();
        this.overlay.update();
    }

    @Override
    public void render(Graphics g) {
        g.setColor(new Color(0x56568E));
        g.fillRect(0,0,Config.WIDTH, Config.HEIGHT);
        super.render(g);
        if(sim.getColonizedPlanet() != null){
            sim.getColonizedPlanet().render(g);
        }
        this.builderGUI.render(g);
        this.overlay.render(g);
    }
}
