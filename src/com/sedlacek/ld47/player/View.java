package com.sedlacek.ld47.player;

import java.awt.*;

public abstract class View {

    public View(){}

    public void update(){}

    public void render(Graphics g){}
}
