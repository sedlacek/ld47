package com.sedlacek.ld47.gui;

import com.sedlacek.ld47.main.Game;
import com.sedlacek.ld47.main.GameObject;
import com.sedlacek.ld47.main.MouseManager;

import java.awt.*;
import java.lang.reflect.Method;

public abstract class GUI extends GameObject {

    protected boolean mouseOver;
    protected Method clickedMethod;
    protected Object invoker;

    @Override
    public void update() {
        if(Game.getMouseRect().intersects(new Rectangle(x, y, w, h))){
            this.mouseOver = true;
            if(Game.getMouseManager().LClicked){
                this.clicked();
                Game.getMouseManager().LClicked = false;
            }
        }else{
            this.mouseOver = false;
        }
    }

    public void clicked(){
    }
}
