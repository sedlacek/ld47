package com.sedlacek.ld47.objects;

import com.sedlacek.ld47.graphics.ImageLoader;

public class LavaPlanet extends Planet {

    public LavaPlanet(int distance){
        super(distance);
        this.img = ImageLoader.loadNS("/planet5.png");
        this.id = 4;
        this.description = new String[]{
                "Bleak planet rich",
                "in minerals, but",
                "very unwelcoming",
                "to any visitors."
        };
        this.bioRepr = 0.1f;
        this.metalRepr = 0.6f;
        this.emptyRepr = 0.2f;
        this.waterRepr = 0.1f;
        super.generateMap();
        this.uraniumChance = 0.75f;
        this.metalChance = 0.5f;
        this.peopleChance = 0.18f;
        this.deathChance = 0.32f;
    }
}
