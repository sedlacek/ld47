package com.sedlacek.ld47.main;

import com.sedlacek.ld47.gui.Menu;
import com.sedlacek.ld47.player.Simulation;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.image.BufferStrategy;
import java.io.InputStream;
import java.util.Random;

import javax.swing.JFrame;

/*
 * @Author: Marek Sedlacek
 * mr.mareksedlacek@gmail.com
 * Twitter: @Sedlacek
 * 
 */

public class Game extends Canvas implements Runnable{
	
	private static final long serialVersionUID = 1L;
	
	public static Rectangle windowRect;
	
	private static int infoFPS;
	private static int infoTicks;
	
	public static boolean soundOn = true;
	public static boolean musicOn = true;
	
	public static Random r = new Random();
	
	private static Game game; 
	public static JFrame frame;
	public static Thread thread;
	
	public static enum State{
		GAME,
		GAME_OVER,
		MENU
	}

	public static enum ViewState {
		SOLAR,
		PLANET
	}
	
	public static State state;
	public static ViewState viewState;

	public static Simulation simulation;
	
	private BufferStrategy bs;
	private Graphics g;
	private static KeyManager keyManager;
	private static MouseManager mouseManager;
	private long lastTime;

	public static boolean paused = false;

	private static final String[] GAME_OVER_TEXT = new String[]{
			"Game over!",
			"F#@K it's over!",
			"You failed!",
	};

	private static final String[] GAME_OVER_SUBTEXT = new String[]{
			"You have been burnt to a crisp...",
			"The sun has destroyed everyone..."
	};

	private static final String[] GAME_OVER_SUBTEXT_FR = new String[]{
			"You're all now ice cubes...",
			"You froze to death"
	};

	private static String goText = GAME_OVER_TEXT[0];
	private static String goSubtext = GAME_OVER_SUBTEXT[0];

	private AudioGalery audioGalery;
	private static boolean mute = false;
	private Menu menu;

	public Game(){
		keyManager = new KeyManager();
		this.addKeyListener(keyManager);
		mouseManager = new MouseManager();
		this.addMouseListener(mouseManager);
		this.addMouseMotionListener(mouseManager);
		this.addMouseWheelListener(mouseManager);
		
		//Sheet = ImageLoader.loadNS("/furniture.png");
		
		//FONTS
		try{
			InputStream font = getClass().getResourceAsStream("/DorFont01.ttf");
			 GraphicsEnvironment ge = 
			         GraphicsEnvironment.getLocalGraphicsEnvironment();
			     ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, font)); 
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			InputStream font = getClass().getResourceAsStream("/DorFont02.ttf");
			 GraphicsEnvironment ge = 
			         GraphicsEnvironment.getLocalGraphicsEnvironment();
			     ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, font)); 
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			InputStream font = getClass().getResourceAsStream("/DorFont03.ttf");
			 GraphicsEnvironment ge = 
			         GraphicsEnvironment.getLocalGraphicsEnvironment();
			     ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, font)); 
		}catch(Exception e){
			e.printStackTrace();
		}

		menu = new Menu();

		state = State.MENU;
		viewState = ViewState.SOLAR;
		simulation = new Simulation(this);

		audioGalery = new AudioGalery();
		AudioGalery.music.loop();
	}
	
	private void update(){
		if(state == State.GAME){
			simulation.update();
		}else if(state == State.MENU){
			menu.update();
		}else if(state == State.GAME_OVER){
			//
		}
		long interval = simulation.speedup ? Config.FIXED_UPDATE_INTERVAL/simulation.speedupAm : Config.FIXED_UPDATE_INTERVAL;
		if(System.currentTimeMillis() - lastTime >= interval){
			this.lastTime = System.currentTimeMillis();
			this.fixedUpdate();
		}
		if(mouseManager.LClicked){
			mouseManager.LClicked = false;
		}
	}

	private void fixedUpdate(){
		// Invoked every second
		if(state == State.GAME){
			simulation.fixedUpdate();
		}
	}

	public static void mute(){
		if(mute){
			AudioGalery.music.loop();
			Config.debug("Start Music");
			musicOn = true;
		}else{
			AudioGalery.music.stopClip();
			Config.debug("Stop Music");
			musicOn = false;
		}
		mute = !mute;
	}
	
	private void input(){
		if(getKeyManager().keys[KeyManager.F1]){
			if(Config.showInfo) {
				Config.showInfo = false;
			}else {
				Config.showInfo = true;
			}
			getKeyManager().keys[KeyManager.F1] = false;
		}
		if(getKeyManager().keys[KeyManager.S]){
			mute();
			getKeyManager().keys[KeyManager.S] = false;
		}
		if(state == State.GAME){
			if(getKeyManager().keys[KeyManager.M]){
				if(viewState == ViewState.PLANET) {
					viewState = ViewState.SOLAR;
				}
				else{
					if(simulation.colonizedIndex > -1) {
						viewState = ViewState.PLANET;
					}
				}
				getKeyManager().keys[KeyManager.M] = false;
			}
		}
	}

	public static void gameOver(boolean froze){
		Game.goText = GAME_OVER_TEXT[r.nextInt(GAME_OVER_TEXT.length)];
		if(froze){
			Game.goSubtext = GAME_OVER_SUBTEXT_FR[r.nextInt(GAME_OVER_SUBTEXT_FR.length)];
		}else {
			Game.goSubtext = GAME_OVER_SUBTEXT[r.nextInt(GAME_OVER_SUBTEXT.length)];
		}
		state = State.GAME_OVER;
	}

	public static void gameOver(String text){
		Game.goText = GAME_OVER_TEXT[r.nextInt(GAME_OVER_TEXT.length)];
		Game.goSubtext = text;
		state = State.GAME_OVER;
	}

	public static void gameWon(){
		Game.goText = "You've escaped the loop!";
		Game.goSubtext = "You're on your way to find a new home...";
		state = State.GAME_OVER;
	}
	
	private void render(){
		bs = this.getBufferStrategy();
		if(bs == null){
			this.createBufferStrategy(2);
			return;
		}
		
		g = bs.getDrawGraphics();
		//Draw
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, Config.WIDTH, Config.HEIGHT);

		if(state == State.GAME || state == State.GAME_OVER){
			simulation.render(g);
		}
		else if(state == State.MENU){
			menu.render(g);
		}
		if(state == State.GAME_OVER){
			g.setColor(new Color(0,0,0,130));
			g.fillRect(0,0,Config.WIDTH, Config.HEIGHT);
			g.setColor(Color.WHITE);
			g.setFont(new Font("DorFont03", Font.PLAIN, 64));
			g.drawString(goText, Config.WIDTH/2-g.getFontMetrics().stringWidth(goText)/2, Config.HEIGHT/2-10);
			g.setColor(Color.WHITE.darker());
			g.setFont(new Font("DorFont03", Font.PLAIN, 32));
			g.drawString(goSubtext, Config.WIDTH/2-g.getFontMetrics().stringWidth(goSubtext)/2, Config.HEIGHT/2+60);
		}
		
		if(Config.showInfo){
			g.setFont(new Font("Consolas", Font.PLAIN, 11));
			g.setColor(Color.WHITE);
			g.drawString("FPS/Ticks: " + infoFPS + "/" + infoTicks, Config.WIDTH-100, 12);
		}
		//Dispose
		bs.show();
		g.dispose();
	}
	
	public static KeyManager getKeyManager(){
		return keyManager;
	}
	
	public static MouseManager getMouseManager(){
		return mouseManager;
	}
	
	public static Rectangle getMouseRect(){
		return mouseManager.getMouseRect();
	}
	
	public void run(){
		this.requestFocus();
		double ns = 1000000000 / Config.fps;
		double delta = 0;
		long now;
		long lastTime = System.nanoTime();
		long timer = 0;
		int ticks = 0;
		int frames = 0;
		
		while(Config.running){
			now = System.nanoTime();
			delta += (now - lastTime) / ns;
			timer += now - lastTime;
			lastTime = now;
			
			if(delta >= 1){
				input();
				update();
				ticks++;
				delta--;
			}
			
			render();
			frames++;
			
			if(timer >= 1000000000){
				//System.out.println(ticks + "   " + frames);
				infoTicks = ticks;
				infoFPS = frames;
				ticks = 0;
				frames = 0;
				timer = 0;
			}
		}
		
		stop();
	}
	
	
	
	public static void main(String [] args){
		if(args.length > 0){
			if(args[0].equals("-d") || args[0].equals("--debug")){
				Config.debugMode = true;
				Config.showInfo = true;
				Config.debug("Debug mode on");
			}
		}

		game = new Game();
		
		frame = new JFrame(Config.NAME);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		/*Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Config.WIDTH=screenSize.Config.WIDTH;
		Config.HEIGHT=screenSize.Config.HEIGHT;
	    frame.setBounds(0,0,screenSize.Config.WIDTH, screenSize.Config.HEIGHT);
	    frame.setUndecorated(true);*/
	    frame.setBounds(0,0, Config.WIDTH, Config.HEIGHT);
	    windowRect = new Rectangle(0, 0, Config.WIDTH, Config.HEIGHT);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
	    
		frame.add(game);
		frame.setVisible(true);
		frame.setAlwaysOnTop(true);
		
		game.start();
	}
	
	public void start(){
		if(Config.running)
			return;
		Config.running = true;
		thread = new Thread(this, "ld47");
		thread.start();
	}
	
	public void stop(){
		if(!Config.running)
			return;
		Config.running = false;
	}
}
