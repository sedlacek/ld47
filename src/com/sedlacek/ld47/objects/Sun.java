package com.sedlacek.ld47.objects;

import com.sedlacek.ld47.main.Config;
import com.sedlacek.ld47.main.Game;
import com.sedlacek.ld47.main.GameObject;

import java.awt.*;
import java.util.Random;

public class Sun extends GameObject {

    private static int MIN_START_RADIUS = 25;
    private static int MAX_START_RADIUS = 30;

    public static int EXPANSION_INTERVAL_FIXED = 10*1000;
    public static int expansionInterval = EXPANSION_INTERVAL_FIXED;// in milliseconds
    public static int expansionRate = 1; // How much will the sun gain per expansion time

    public int radius;
    private long lastTime;

    private Random r;

    public Sun(){
        this.r = new Random();
        this.radius = r.nextInt(MAX_START_RADIUS-MIN_START_RADIUS)+MIN_START_RADIUS;
        lastTime = System.currentTimeMillis();
    }

    @Override
    public void update() {
        if(System.currentTimeMillis() - lastTime >= expansionInterval){
            this.radius += expansionRate;
            this.lastTime = System.currentTimeMillis();
            if(radius >= 101 && Game.simulation.planets[0] != null){
                Game.simulation.destroyPlanet(0);
            }
            else if(radius >= 201 && Game.simulation.planets[1] != null){
                Game.simulation.destroyPlanet(1);
            }
            else if(radius >= 301 && Game.simulation.planets[2] != null){
                Game.simulation.destroyPlanet(2);
            }
            else if(radius >= 401 && Game.simulation.planets[3] != null){
                Game.simulation.destroyPlanet(3);
            }
        }
    }

    @Override
    public void render(Graphics g) {
        g.setColor(new Color(255, 187, 51));
        g.fillOval(Config.WIDTH/2-radius, Config.HEIGHT/2-radius, radius*2, radius*2);
    }
}
