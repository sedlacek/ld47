package com.sedlacek.ld47.world;

import com.sedlacek.ld47.gui.Button;
import com.sedlacek.ld47.gui.GUI;
import com.sedlacek.ld47.main.Config;
import com.sedlacek.ld47.main.Game;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public abstract class Station extends GUI {

    protected BufferedImage image;
    protected String name;
    protected String[] description;
    public static final int MULTIPLIER = 7;
    protected Tile tile;
    protected Button actionButton;
    protected boolean active = false;
    protected int price;
    protected int gain;
    protected int crewSize;

    @Override
    public void update() {
        super.update();
        if(this.crewSize > 0) {
            if (!active) {
                actionButton.text = "Assign (-" + crewSize + ")";
            } else {
                actionButton.text = "Abandon (+" + crewSize + ")";
            }
        }
        if(!this.active && this.crewSize > Game.simulation.people-Game.simulation.working){
            this.actionButton.disabled = true;
        }else{
            this.actionButton.disabled = false;
        }
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(this.image, x, y, w, h, null);
    }

    public void action(){
        if(this.active){
            Game.simulation.working -= crewSize;
        }else{
            Game.simulation.working += crewSize;
        }
        this.active = !active;
    }

    public String getName() {
        return name;
    }

    public String[] getDescription() {
        return description;
    }

    public Button getActionButton() {
        return actionButton;
    }

    public boolean isActive() {
        return active;
    }

    public int getPrice() {
        return price;
    }

    public int getCrewSize() {
        return crewSize;
    }
}
