package com.sedlacek.ld47.gui;

import com.sedlacek.ld47.main.Config;
import com.sedlacek.ld47.main.Game;
import com.sedlacek.ld47.main.GameObject;
import com.sedlacek.ld47.objects.Planet;
import com.sedlacek.ld47.player.Simulation;

import java.awt.*;

public class PlanetInfo extends GameObject {

    private Planet p;
    private Font font = new Font("DorFont03", Font.BOLD, 16);
    private Color backgroundC = Config.yellow;
    private Button bColonize;
    private int index;
    private Simulation sim;

    private int xoffset = 5;
    private int yoffset = 25;
    private int space = 15;
    private Font titleFont = new Font("DorFont03", Font.BOLD, 16);
    private Color fontColor = Color.BLACK;

    public PlanetInfo(Simulation sim, Planet p, int index){
        this.p = p;
        this.w = 160;
        this.h = p.getDescription().length*space+70+30;
        this.x = p.getX()+p.getW()+20;
        this.y = p.getY();
        this.index = index;
        this.sim = sim;

        try {
            this.bColonize = new Button(p.isColonized() ? "Abandon" : "Colonize", x, y+h, 90,25,
                                        Config.yellow.darker().darker(), Color.BLACK,
                                        this.getClass().getDeclaredMethod("clicked"), this);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update() {
        this.x = p.getX()+p.getW()+15;
        this.y = p.getY();

        if(y + h > Config.HEIGHT - 35){
            this.y = p.getY()-h+p.getH();
        }
        if(x + w > Config.WIDTH - 20){
            this.x = p.getX()-w-15;
        }

        this.bColonize.setX(this.x+5);
        this.bColonize.setY(this.y+h-30);
        this.bColonize.update();
        if(p.isColonized()){
            this.bColonize.text = "Exit orbit";
            if(sim.ship.getPlanetIndex() == 3 && sim.getColonizedPlanet() == p){
                this.bColonize.text = "Exit galaxy";
                this.bColonize.setW(105);

            }
        }else{
            this.bColonize.text = "Colonize";
        }
        if(Game.paused || sim.ship.getPlanetIndex() != index || sim.colonizedIndex != -1){
            this.bColonize.disabled = true;
        }
        else{
            if(sim.ship.getPlanetIndex() == 3 && sim.getColonizedPlanet() == p) {
                if(sim.uranium >= Simulation.URANIUM_NEEDED){
                    this.bColonize.disabled = false;
                }
                else{
                    this.bColonize.disabled = true;
                }
            }else{
                this.bColonize.disabled = false;
            }
        }
    }

    @Override
    public void render(Graphics g) {
        g.setColor(backgroundC);
        g.fillRoundRect(x, y, w, h, 20, 20);

        g.setFont(titleFont);
        g.setColor(fontColor);
        g.drawString(p.getName(), x+xoffset, y+yoffset);
        if(p.isColonized()) {
            g.setColor(new Color(0x319700));
            g.drawString("Colonized", x+xoffset, y+yoffset+20);
        }else{
            g.setColor(new Color(0xBB0000));
            g.drawString("Not colonized", x+xoffset, y+yoffset+20);
        }
        g.setFont(font);
        g.setColor(fontColor);
        for(int i = 0; i < p.getDescription().length; i++) {
            g.drawString(p.getDescription()[i], x+xoffset, y+yoffset+space*i+30+20);
        }

        this.bColonize.render(g);
    }

    @Override
    public void clicked() {
        if(!p.isColonized()) {
            sim.colonize(index);
        }else{
            sim.exitOrbit(index);
        }
    }
}
