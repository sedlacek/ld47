package com.sedlacek.ld47.world;

import com.sedlacek.ld47.graphics.ImageLoader;
import com.sedlacek.ld47.main.Game;
import com.sedlacek.ld47.objects.Planet;

public class TileBio extends Tile{

    public static final int ID = 1;

    public TileBio(Planet p, int col, int row){
        super(p, col, row);
        this.img = ImageLoader.loadNS(p.getTileFiles().get("BIO"));
        this.id = ID;
    }

    @Override
    public void update() {
        super.update();
        if(Game.simulation.planetView.builderGUI.stationBuilder != null){
            if(Game.simulation.planetView.builderGUI.selectedStation == Greenhouse.class){
                this.buildable = true;
            }
        }
        else{
            buildable = false;
        }
    }
}
