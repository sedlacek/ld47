package com.sedlacek.ld47.objects;

import com.sedlacek.ld47.graphics.ImageLoader;

public class DesertPlanet extends Planet {

    public DesertPlanet(int distance) {
        super(distance);
        this.img = ImageLoader.loadNS("/planet2.png");
        this.id = 1;
        this.description = new String[]{
                "Desolate hot pla-",
                "net without much",
                "water and green-",
                "ery, but rich in",
                "natural gases.",
        };
        this.bioRepr = 0.1f;
        this.metalRepr = 0.2f;
        this.emptyRepr = 0.6f;
        this.waterRepr = 0.1f;
        super.generateMap();
        this.uraniumChance = 0.60f;
        this.metalChance = 0.3f;
        this.peopleChance = 0.22f;
        this.deathChance = 0.3f;
    }
}
