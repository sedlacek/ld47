package com.sedlacek.ld47.gui;

import com.sedlacek.ld47.graphics.ImageLoader;
import com.sedlacek.ld47.main.Config;
import com.sedlacek.ld47.main.Game;
import com.sedlacek.ld47.player.Simulation;
import com.sun.security.auth.login.ConfigFile;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class MainOverlay {

    private static Button bPause;
    private Simulation sim;
    private static Button bMap;
    private static Button bMute;
    private static Button bSpeedup;

    private static final BufferedImage mapIconImg = ImageLoader.loadNS("/iconMap.png");
    private static final BufferedImage oxygenIconImg = ImageLoader.loadNS("/oxygenIcon.png");
    private static final BufferedImage foodIconImg = ImageLoader.loadNS("/foodIcon.png");
    private static final BufferedImage metalIconImg = ImageLoader.loadNS("/metalIcon.png");
    private static final BufferedImage gasIconImg = ImageLoader.loadNS("/fuelIcon.png");
    private static final BufferedImage peopleIconImg = ImageLoader.loadNS("/peopleIcon.png");
    private static final BufferedImage uraniumIconImg = ImageLoader.loadNS("/uraniumIcon.png");

    private static final BufferedImage thermometerImg = ImageLoader.loadNS("/thermometer.png");

    private static final Font infoFont = new Font("DorFont03", Font.PLAIN, 16);
    private static final Color infoColor = Color.BLACK;

    public ArrayList<Notification> notifications;

    private int multiplier = 2;
    private int space = 32;
    private int xoffs = 10;
    private int yoffs = 10;

    public MainOverlay(Simulation sim){
        this.sim = sim;
        this.notifications = new ArrayList<>();
        try {
            bPause = new Button("||", 10, 10, 36, 36, /*new Color(0xD063FF)*/Config.yellow,
                    Color.BLACK, this.getClass().getDeclaredMethod("pauseClicked"),
                    this);
            bPause.yoffsetText = -3;

            bSpeedup = new Button(">>", 10, 10+(36+10)*1, 36, 36, Config.yellow,
                    Color.BLACK, this.getClass().getDeclaredMethod("speedUpClicked"),
                    this);
            bSpeedup.yoffsetText = -3;

            bMap = new Button("", 10, 10+(36+10)*2, 36, 36, /*new Color(0xD063FF)*/Config.yellow,
                    Color.BLACK, this.getClass().getDeclaredMethod("mapClicked"),
                    this);
            bMap.setIcon(mapIconImg);
            bMap.multiplier = 2;


            bMute = new Button("M", 10, 10+(36+10)*3, 36, 36, Config.yellow,
                    Color.BLACK, this.getClass().getDeclaredMethod("muteClicked"),
                    this);
            bMute.yoffsetText = -3;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public void update(){
        if(notifications.size() > 0){
            notifications.get(0).update();
        }else {
            bPause.update();
            bMap.update();
            bMute.update();
            bSpeedup.update();
            if(sim.speedup){
                bSpeedup.active = true;
                bSpeedup.color = Config.purple;
            }
            else{
                bSpeedup.active = false;
                bSpeedup.color = Config.yellow;
            }
            if(Game.musicOn){
                bMute.active = false;
            }
            else{
                bMute.active = false;
            }
            if(Game.paused){
                bPause.color = Config.purple;
            }
            else{
                bPause.color = Config.yellow;
            }
            if (Game.viewState == Game.ViewState.PLANET) {
                bMap.setIcon(mapIconImg);
            } else {
                if (sim.getColonizedPlanet() != null)
                    bMap.setIcon(sim.getColonizedPlanet().getImg());
                else {
                    bMap.setIcon(null);
                }
            }
        }
    }

    public void render(Graphics g){
        g.setColor(Config.yellow);
        int wi = 10*16*multiplier+space*4+2*xoffs+20;
        int hi = 16*multiplier+yoffs*2;
        int xi = Config.WIDTH/2-wi/2;
        g.fillRoundRect(xi, -10, wi, hi+10, 15, 15);

        g.drawImage(oxygenIconImg, xi+xoffs, yoffs, oxygenIconImg.getWidth()*multiplier,
                oxygenIconImg.getHeight()*multiplier, null);
        g.drawImage(metalIconImg, xi+xoffs+wi/6, yoffs, metalIconImg.getWidth()*multiplier,
                metalIconImg.getHeight()*multiplier, null);
        g.drawImage(foodIconImg, xi+xoffs+(wi/6)*2, yoffs, foodIconImg.getWidth()*multiplier,
                foodIconImg.getHeight()*multiplier, null);
        g.drawImage(gasIconImg, xi+xoffs+(wi/6)*3, yoffs, gasIconImg.getWidth()*multiplier,
                gasIconImg.getHeight()*multiplier, null);
        g.drawImage(peopleIconImg, xi+xoffs+(wi/6)*4, yoffs, peopleIconImg.getWidth()*multiplier,
                peopleIconImg.getHeight()*multiplier, null);
        g.drawImage(uraniumIconImg, xi+xoffs+(wi/6)*5, yoffs, uraniumIconImg.getWidth()*multiplier,
                uraniumIconImg.getHeight()*multiplier, null);

        g.setColor(infoColor);
        g.setFont(infoFont);
        g.drawString(formatNumber(sim.o2), xi+xoffs+16*multiplier+5, yoffs+12*multiplier);
        g.drawString(formatNumber(sim.metal), xi+xoffs+(wi/6)+16*multiplier+5, yoffs+12*multiplier);
        g.drawString(formatNumber(sim.food), xi+xoffs+(wi/6)*2+16*multiplier+5, yoffs+12*multiplier);
        g.drawString(formatNumber(sim.gas), xi+xoffs+(wi/6)*3+16*multiplier+5, yoffs+12*multiplier);
        g.drawString(formatNumber(sim.people-sim.working), xi+xoffs+(wi/6)*4+16*multiplier+12, yoffs+12*multiplier-10);
        g.drawString("/"+formatNumber(sim.people), xi+xoffs+(wi/6)*4+16*multiplier+5, yoffs+12*multiplier+5);
        g.drawString(formatNumber(sim.uranium), xi+xoffs+(wi/6)*5+16*multiplier+12, yoffs+12*multiplier-10);
        g.drawString("/"+formatNumber(Simulation.URANIUM_NEEDED), xi+xoffs+(wi/6)*5+16*multiplier+5, yoffs+12*multiplier+5);

        int tmmult = 2;
        int tmx = Config.WIDTH-thermometerImg.getWidth()*tmmult-10;
        int tmy = 10;

        g.setColor(Config.yellow);
        g.fillRect(tmx+10, tmy+10, thermometerImg.getWidth()*tmmult-20, thermometerImg.getHeight()*tmmult-20);

        if(sim.temperature < 0){
            g.setColor(new Color(0x0F0FEC));
        }
        else{
            g.setColor(new Color(0xC20000));
        }
        int height = (sim.temperature+400)/(800/(48*tmmult));
        if(height < 0){
            height = 0;
        }
        g.fillRect(tmx+4*tmmult, tmy+thermometerImg.getHeight()*tmmult-14*tmmult-height, 18*tmmult, height);
        //Tempterature

        g.drawImage(thermometerImg, tmx, tmy, thermometerImg.getWidth()*tmmult, thermometerImg.getHeight()*tmmult, null);
        g.setColor(Color.BLACK);
        g.drawString(""+sim.temperature+" C",
                tmx+thermometerImg.getWidth()*multiplier/2-g.getFontMetrics().stringWidth(""+sim.temperature+" C")/2,
                tmy+thermometerImg.getHeight()*tmmult-5);

        if(Game.paused) {
            g.setColor(new Color(0,0,0,50));
            g.fillRect(0,0, Config.WIDTH, Config.HEIGHT);
            g.setFont(new Font("DorFont03", Font.BOLD, 32));
            g.setColor(Color.WHITE);
            g.drawString("Time paused", Config.WIDTH/2-g.getFontMetrics().stringWidth("Time paused")/2,
                    Config.HEIGHT-40-120);
        }
        else if(sim.speedup) {
            g.setFont(new Font("DorFont03", Font.BOLD, 32));
            g.setColor(new Color(255,255,255,120));
            g.drawString("Sped up time", 10+(36+10)*1,
                    (36+10)+36+5);
        }
        bPause.render(g);
        bSpeedup.render(g);
        bMap.render(g);
        bMute.render(g);

        if(notifications.size() > 0){
            notifications.get(0).render(g);
        }
    }

    private String formatNumber(int n){
        float conv = (float)n;
        if(n >= 1000000){
            conv/=1000000;
            return String.format("%.1fM", conv);
        }
        else if(n >= 1000){
            conv/=1000;
            return String.format("%.1fk", conv);
        }
        return n+"";
    }

    public void pauseClicked(){
        Config.debug("Game paused");
        Game.paused = !Game.paused;
        if(Game.paused){
            bPause.text = "|>";
            bPause.active = true;
        }
        else{
            bPause.text = "||";
            bPause.active = false;
        }
    }

    public void muteClicked(){
        Game.mute();
    }

    public void mapClicked(){
        if(Game.viewState == Game.ViewState.PLANET) {
            Game.viewState = Game.ViewState.SOLAR;
        }
        else{
            if(Game.simulation.colonizedIndex > -1) {
                Game.viewState = Game.ViewState.PLANET;
            }
        }
    }

    public void speedUpClicked(){
        this.sim.speedUpTime();
    }
}
