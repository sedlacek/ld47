package com.sedlacek.ld47.gui;

import com.sedlacek.ld47.main.Game;
import com.sedlacek.ld47.main.KeyManager;
import com.sedlacek.ld47.player.Simulation;
import com.sedlacek.ld47.world.Station;

import java.awt.*;

public class PlanetOverlay {

    StationInfo stationInfo;
    Station selectedStation;
    Simulation sim;

    public PlanetOverlay(Simulation s){
        this.sim = s;
    }

    public void update(){
        if(selectedStation != null){
            stationInfo.update();
        }
        if(Game.getKeyManager().keys[KeyManager.ESC] && stationInfo != null){
            selectedStation = null;
            stationInfo = null;
        }
    }

    public void render(Graphics g){
        if(selectedStation != null){
            stationInfo.render(g);
        }
    }

    public void stationClicked(Station s){
        this.selectedStation = s;
        if(s != null)
            stationInfo = new StationInfo(sim, s);
    }

    public void closeInfo(){
        stationInfo = null;
        selectedStation = null;
    }
}
