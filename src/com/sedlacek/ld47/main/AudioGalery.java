package com.sedlacek.ld47.main;

import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import java.util.Random;
import java.util.stream.StreamSupport;

public class AudioGalery {

    public static AudioPlayer goodNotification = new AudioPlayer("/soundNotif.wav");
    public static final AudioPlayer badNotification = new AudioPlayer("/soundNotifBad.wav");
    public static final AudioPlayer tutorialNotification = new AudioPlayer("/soundTutorial2.wav");

    public static AudioPlayer button = new AudioPlayer("/soundButton2.wav");

    public static AudioPlayer music = new AudioPlayer("/music.wav");
}
