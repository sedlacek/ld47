package com.sedlacek.ld47.gui;

import com.sedlacek.ld47.main.AudioGalery;
import com.sedlacek.ld47.player.Simulation;

public class Tutorial {

    private boolean firstColonization, firstBuilding, clickedPod, planetExited;
    private Simulation sim;
    public int buildAm = 0;
    public static boolean disableTuts;

    public Tutorial(Simulation sim){
        this.sim = sim;
    }

    public void start(){
        if(!disableTuts) {
            Notification n = new Notification(
                    "Hello, captain!",
                    new String[]{
                            "We're stuck in a loop!..literally...",
                            "We need to leave this solar system.",
                            "The sun is rapidly expanding...",
                            "Unfortunately to jump to hyperspace",
                            "we need to get to the edge of the",
                            "solar system."
                    },
                    "I see...");
            n.setSound(AudioGalery.tutorialNotification);
            sim.addNotification(n);
            n = new Notification(
                    "...And there's more!",
                    new String[]{
                            "We're all out of uranium for our",
                            "hyperdrive engine. It looks like",
                            "we'll need to get some on our way",
                            "to the solar edge."
                    },
                    "oh.");
            n.setSound(AudioGalery.tutorialNotification);
            sim.addNotification(n);
            n = new Notification(
                    "Let's not waste more time!",
                    new String[]{
                            "Click on the planet we're orbiting",
                            "and colonize it!"
                    },
                    "Will do!");
            n.setSound(AudioGalery.tutorialNotification);
            sim.addNotification(n);
        }
    }

    public void colonized(){
        if(firstColonization || disableTuts)
            return;
        else{
            Notification n = new Notification(
                    "New home",
                    new String[]{
                            "You did it! We set up oxygen perimeter",
                            "and the land inside of it should be used",
                            "to get more resources so we can reach",
                            "the solar edge!"
                    },
                    "Of course");
            n.setSound(AudioGalery.tutorialNotification);
            sim.addNotification(n);
            n = new Notification(
                    "Just a tip",
                    new String[]{
                            "I know that you're the boss, but",
                            "if it was on me... I would built a",
                            "metal refinery first, so that we can",
                            "get more material quickly."
                    },
                    "I was just about to do that");
            n.setSound(AudioGalery.tutorialNotification);
            sim.addNotification(n);
            firstColonization = true;
        }
    }

    public void firstBuild(){
        buildAm++;
        if(buildAm == 2){
            exploreTip();
        }
        if(firstBuilding || disableTuts)
            return;
        Notification n = new Notification(
                "Oh wow!",
                new String[]{
                        "Look at you! A born colonizer!",
                        "If you ever need to move your",
                        "people to different station,",
                        "just click on occupied station",
                        "and you can whenever abandon it",
                        "or again assign people to it."
                },
                "Ah, yes.");
        n.setSound(AudioGalery.tutorialNotification);
        sim.addNotification(n);
        this.firstBuilding = true;
    }

    public void exploreTip(){
        if(disableTuts)
            return;
        Notification n = new Notification(
                "One more thing",
                new String[]{
                        "You should also try exploring the",
                        "planet a bit. That's the way to",
                        "find what we need the most -",
                        "Uranium. Just click the compass",
                        "icon and it will send some people",
                        "to scavenge. Just beware that the",
                        "more hostile environment, the worse",
                        "odds of survival for your scavengers."
                },
                "Hmmm");
        n.setSound(AudioGalery.tutorialNotification);
        sim.addNotification(n);
        n = new Notification(
                "...Speaking of which",
                new String[]{
                        "Isn't the sun just majestic?",
                        "Too bad it will kill us in no",
                        "time if we don't get off of this",
                        "planet.",
                },
                "...");
        n.setSound(AudioGalery.tutorialNotification);
        sim.addNotification(n);
        n = new Notification(
                "Just check it out.",
                new String[]{
                        "To see how much has the sun expanded",
                        "just open solar map. You can",
                        "switch between maps with top left",
                        "button or by pressing [M]."
                },
                "I shall do that now!");
        n.setSound(AudioGalery.tutorialNotification);
        sim.addNotification(n);
    }

    public void travelPod(){
        if(clickedPod || disableTuts)
            return;
        Notification n = new Notification(
                "This is our way out",
                new String[]{
                        "This bad boy will lift us off of",
                        "this planet, back to our ship.",
                        "we just need to get enough fuel",
                        "to fly it up there."
                },
                "Nice");
        n.setSound(AudioGalery.tutorialNotification);
        sim.addNotification(n);
        clickedPod = true;
    }

    public void exitPlanet(){
        if(planetExited || disableTuts)
            return;
        Notification n = new Notification(
                "We did it!",
                new String[]{
                        "We got back to our ship and",
                        "with all these goods! We can",
                        "now fly to other planet and",
                        "harvest it's resources!",
                        "Just click on our current planet,",
                        "and click \"Exit orbit\"!"
                },
                "Nice");
        n.setSound(AudioGalery.tutorialNotification);
        sim.addNotification(n);
        planetExited = true;
    }
}
