package com.sedlacek.ld47.gui;

import com.sedlacek.ld47.graphics.ImageLoader;
import com.sedlacek.ld47.main.Config;
import com.sedlacek.ld47.main.Game;
import com.sedlacek.ld47.main.GameObject;
import com.sedlacek.ld47.player.Simulation;
import com.sedlacek.ld47.world.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.lang.reflect.InvocationTargetException;

public class BuilderGUI extends GameObject {

    private Simulation sim;
    private static final BufferedImage drillImg = ImageLoader.loadNS("/drill2.png");
    private static final BufferedImage o2GenImg = ImageLoader.loadNS("/o2gen.png");
    private static final BufferedImage metalRefImg = ImageLoader.loadNS("/metalRefinery.png");
    private static final BufferedImage ghouseImg = ImageLoader.loadNS("/greenhouse.png");

    private static final BufferedImage compassImg = ImageLoader.loadNS("/compassIcon.png");

    private static final BufferedImage drillImgTr = ImageLoader.loadNS("/drill2Tr.png");
    private static final BufferedImage o2GenImgTr = ImageLoader.loadNS("/o2genTr.png");
    private static final BufferedImage metalRefImgTr = ImageLoader.loadNS("/metalRefineryTr.png");
    private static final BufferedImage ghouseImgTr = ImageLoader.loadNS("/greenhouseTr.png");

    private static final BufferedImage compassImgTr = ImageLoader.loadNS("/compassIconTr.png");

    public StationBuilder stationBuilder;
    public Class selectedStation;

    private final int multiplier = 5;
    private final int space = 10;
    private final int xoffs = 10;

    private boolean building = false;

    private Button bDrill, bO2Gen, bMetalRef, bGhouse, bCompass;

    public BuilderGUI(Simulation sim){
        this.sim = sim;
        this.h = 100+space*2;
        this.w = 100*5+space*2+4*space+xoffs*2;
        this.x = Config.WIDTH/2-w/2;
        this.y = Config.HEIGHT-h-space*3;

        try {
            bDrill = new Button(""+Drill.PRICE, x+space+xoffs, y+space, 100, 100, Config.yellow,
                    Color.BLACK, this.getClass().getDeclaredMethod("drillClicked"),
                    this);
            bDrill.setIcon(drillImg);
            bDrill.setDisabledIcon(drillImgTr);
            bDrill.multiplier = multiplier;
            bDrill.xoffsetText = -35;

            bGhouse = new Button(""+Greenhouse.PRICE, x+space*2+100+xoffs, y+space, 100, 100, Config.yellow,
                    Color.BLACK, this.getClass().getDeclaredMethod("ghouseClicked"),
                    this);
            bGhouse.setIcon(ghouseImg);
            bGhouse.setDisabledIcon(ghouseImgTr);
            bGhouse.multiplier = multiplier;
            bGhouse.xoffsetText = -35;

            bO2Gen = new Button(""+O2Generator.PRICE, x+space*3+200+xoffs, y+space, 100, 100, Config.yellow,
                    Color.BLACK, this.getClass().getDeclaredMethod("o2GenClicked"),
                    this);
            bO2Gen.setIcon(o2GenImg);
            bO2Gen.setDisabledIcon(o2GenImgTr);
            bO2Gen.multiplier = multiplier;
            bO2Gen.xoffsetText = -35;

            bMetalRef = new Button(""+MetalRefinery.PRICE, x+space*4+300+xoffs, y+space, 100, 100, Config.yellow,
                    Color.BLACK, this.getClass().getDeclaredMethod("metalRefClicked"),
                    this);
            bMetalRef.setIcon(metalRefImg);
            bMetalRef.setDisabledIcon(metalRefImgTr);
            bMetalRef.multiplier = multiplier;
            bMetalRef.xoffsetText = -35;

            //
            bCompass = new Button(""+sim.explorationSize+" people", x+space*5+400+xoffs, y+space, 100, 100, Config.yellow,
                    Color.BLACK, this.getClass().getDeclaredMethod("explorationClicked"),
                    this);
            bCompass.setIcon(compassImg);
            bCompass.setDisabledIcon(compassImgTr);
            bCompass.multiplier = multiplier;
            bCompass.xoffsetText = -10;
            bCompass.textColor = Color.WHITE.darker().darker().darker().darker();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(){
        if(building || Game.paused){
            bDrill.disabled = true;
            bGhouse.disabled = true;
            bMetalRef.disabled = true;
            bO2Gen.disabled = true;
            bCompass.disabled = true;
        }else {
            if (sim.metal < Drill.PRICE) {
                bDrill.disabled = true;
            }
            else{
                bDrill.disabled = false;
            }
            if (sim.metal < Greenhouse.PRICE) {
                bGhouse.disabled = true;
            }
            else{
                bGhouse.disabled = false;
            }
            if (sim.metal < MetalRefinery.PRICE) {
                bMetalRef.disabled = true;
            }
            else{
                bMetalRef.disabled = false;
            }
            if (sim.metal < O2Generator.PRICE) {
                bO2Gen.disabled = true;
            }
            else{
                bO2Gen.disabled = false;
            }
            if (sim.exploring > 0 || sim.people-sim.working < sim.explorationSize) {
                bCompass.disabled = true;
            }
            else{
                bCompass.disabled = false;
            }
        }
        bDrill.update();
        bGhouse.update();
        bMetalRef.update();
        bO2Gen.update();
        bCompass.update();
        if(stationBuilder != null){
            stationBuilder.update();
        }
    }

    @Override
    public void render(Graphics g){
        g.setColor(Config.yellow);
        g.fillRoundRect(x, y, w, h+100, 15, 15);

        bDrill.render(g);
        bGhouse.render(g);
        bMetalRef.render(g);
        bO2Gen.render(g);
        bCompass.render(g);

        if(stationBuilder != null){
            stationBuilder.render(g);
        }
    }

    public void build(Tile t){
        if(t.isBuildable() && t.getStation() == null){
            try {
                Station s = (Station)selectedStation.getDeclaredConstructor(Tile.class).newInstance(t);
                t.addStation(s);
                sim.getColonizedPlanet().getStations().add(s);
                sim.metal -= s.getPrice();
                if(sim.people - sim.working >= s.getCrewSize()) {
                    s.action();
                }
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
            Game.simulation.tutorial.firstBuild();
        }
        endStationBuilder();
    }

    public void endStationBuilder(){
        this.stationBuilder = null;
        this.building = false;
    }

    public void drillClicked(){
        this.selectedStation = Drill.class;
        this.stationBuilder = new StationBuilder(drillImgTr, 7);
        this.building = true;
        sim.planetView.overlay.selectedStation = null;
        sim.planetView.overlay.stationInfo = null;
    }

    public void o2GenClicked(){
        this.selectedStation = O2Generator.class;
        this.stationBuilder = new StationBuilder(o2GenImgTr, 7);
        this.building = true;
        sim.planetView.overlay.selectedStation = null;
        sim.planetView.overlay.stationInfo = null;
    }

    public void ghouseClicked(){
        this.selectedStation = Greenhouse.class;
        this.stationBuilder = new StationBuilder(ghouseImgTr, 7);
        this.building = true;
        sim.planetView.overlay.selectedStation = null;
        sim.planetView.overlay.stationInfo = null;
    }

    public void metalRefClicked(){
        this.selectedStation = MetalRefinery.class;
        this.stationBuilder = new StationBuilder(metalRefImgTr, 7);
        this.building = true;
        sim.planetView.overlay.selectedStation = null;
        sim.planetView.overlay.stationInfo = null;
    }

    public void explorationClicked(){
        sim.startExploration();
    }

}
