package com.sedlacek.ld47.world;

import com.sedlacek.ld47.graphics.ImageLoader;
import com.sedlacek.ld47.main.Game;
import com.sedlacek.ld47.objects.Planet;

public class TileWater extends Tile{

    public static final int ID = 2;

    public TileWater(Planet p, int col, int row){
        super(p, col, row);
        this.img = ImageLoader.loadNS(p.getTileFiles().get("WATER"));
        this.id = ID;
    }

    @Override
    public void update() {
        super.update();
        if(Game.simulation.planetView.builderGUI.stationBuilder != null){
            if(Game.simulation.planetView.builderGUI.selectedStation == O2Generator.class){
                this.buildable = true;
            }
        }
        else{
            buildable = false;
        }
    }
}
