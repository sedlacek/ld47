package com.sedlacek.ld47.world;

import com.sedlacek.ld47.graphics.ImageLoader;
import com.sedlacek.ld47.gui.Button;
import com.sedlacek.ld47.main.Config;
import com.sedlacek.ld47.main.Game;

import java.awt.*;

public class MetalRefinery extends Station{

    public static final int PRICE = 60;
    private int ticks = 0;

    public MetalRefinery(Tile tile){
        this.name = "Metal refinery     ";
        this.description = new String[]{
                "Used to extract",
                "and smelt metals",
                "into usable form.",
        };
        try {
            actionButton = new Button("Assign", 0, 0, 130, 25, Config.yellow.darker().darker(),
                    Color.BLACK, this.getClass().getDeclaredMethod("action"), this);
            actionButton.disableOnPause = true;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        this.image = ImageLoader.loadNS("/metalRefinery.png");
        this.tile = tile;
        this.w = 12*MULTIPLIER;
        this.h = 15*MULTIPLIER;
        this.x = tile.getX()+tile.getW()/2-w/2;
        this.y = tile.getY()+tile.getH()/2-h+4*MULTIPLIER;
        this.price = PRICE;
        this.crewSize = 10;
        this.gain = 4;
    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public void fixedUpdate(){
        if(this.active) {
            ticks++;
            if (ticks >= 2) {
                Game.simulation.metal += gain;
                ticks = 0;
            }
        }
    }

    @Override
    public void action() {
        super.action();
    }
}
