package com.sedlacek.ld47.gui;

import com.sedlacek.ld47.main.Config;
import com.sedlacek.ld47.objects.Planet;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class PlanetButton extends GUI {

    private Planet p;
    public boolean active = false;
    private SolarOverlay inv;

    public PlanetButton(Planet p, Method m, SolarOverlay invoker){
        this.p = p;
        this.clickedMethod = m;
        this.inv = invoker;
    }

    @Override
    public void update() {
        super.update();
        this.x = p.getX();
        this.y = p.getY();
        this.w = p.getW();
        this.h = p.getH();
    }

    @Override
    public void render(Graphics g) {
    }

    @Override
    public void clicked() {
        if(clickedMethod != null){
            try {
                active = !active;
                clickedMethod.invoke(this.inv);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }
}
