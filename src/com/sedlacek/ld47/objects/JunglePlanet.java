package com.sedlacek.ld47.objects;

import com.sedlacek.ld47.graphics.ImageLoader;

public class JunglePlanet extends Planet {

    public JunglePlanet(int distance){
        super(distance);
        this.img = ImageLoader.loadNS("/planet3.png");
        this.id = 2;
        this.description = new String[]{
                "Planet overtaken",
                "by plants and",
                "other eukaryotic",
                "life forms."
        };
        this.bioRepr = 0.6f;
        this.metalRepr = 0.1f;
        this.emptyRepr = 0.1f;
        this.waterRepr = 0.2f;
        super.generateMap();
        this.uraniumChance = 0.50f;
        this.metalChance = 0.3f;
        this.peopleChance = 0.42f;
        this.deathChance = 0.21f;
    }
}
