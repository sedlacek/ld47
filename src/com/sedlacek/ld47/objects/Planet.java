package com.sedlacek.ld47.objects;

import com.sedlacek.ld47.main.Config;
import com.sedlacek.ld47.main.Game;
import com.sedlacek.ld47.main.GameObject;
import com.sedlacek.ld47.player.View;
import com.sedlacek.ld47.world.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

public abstract class Planet extends GameObject {

    protected BufferedImage img;
    protected int distance;
    protected float angle;
    protected float speed;
    protected int multiplier;
    private static Random r;
    protected String name;
    protected String[] description;
    protected boolean colonized = false;
    protected int id;
    protected HashMap<String, String> tileFiles;

    public static final int MAX_TILES_SIZE = 6;

    public static final int MAX_TILES = 19;
    public static final int MIN_TILES = 14;

    protected float uraniumChance, peopleChance, metalChance, deathChance;

    protected float bioRepr, metalRepr, waterRepr, emptyRepr;
    protected int extraTileAm;
    protected int extraTileID;

    protected Tile[][] tiles;
    protected ArrayList<Station> stations;

    private static final String[] PLANET_NAMES = {
            "PJ-42", "Gantoo", "Iridium", "A2", "Scrifter",
            "Bao 85", "Jaavaa", "L1-nux", "Noui*", "Afarii",
    };
    private static ArrayList<String> avaliableNames = new ArrayList<String>(Arrays.asList(PLANET_NAMES));

    public Planet(int distance){
        this.name = getRandomName();
        r = new Random();
        this.distance = distance;
        this.angle = r.nextInt(360);
        this.speed = r.nextFloat()+0.5f;
        this.multiplier = 3;
        this.stations = new ArrayList<>();
        this.tileFiles = new HashMap<>();
        fillHashMap("/tileDirt.png", "/tileMetal.png", "/tileGrass.png", "/tileWater.png"); //TODO: move to designated class
        this.tiles = new Tile[MAX_TILES_SIZE][MAX_TILES_SIZE];
    }

    public void fillHashMap(String empty, String metal, String bio, String water){
        this.tileFiles.put("EMPTY", empty);
        this.tileFiles.put("METAL", metal);
        this.tileFiles.put("BIO", bio);
        this.tileFiles.put("WATER", water);
    }

    public void generateMap(){
        /*for(int c = 0; c < MAX_TILES_SIZE; c++){
            for(int rw = 0; rw < MAX_TILES_SIZE; rw++){
                tiles[c][rw] = new TileEmpty(this, c, rw);
            }
        }*/
        int tileAm = r.nextInt(MAX_TILES-MIN_TILES)+MIN_TILES;
        int tx = MAX_TILES_SIZE/2;
        int ty = MAX_TILES_SIZE/2;

        ArrayList<Integer> tilePool = new ArrayList<>();
        for(int i = 0; i < Math.ceil(tileAm*emptyRepr); i++) {
            tilePool.add(TileEmpty.ID);
        }
        for(int i = 0; i < Math.ceil(tileAm*waterRepr); i++) {
            tilePool.add(TileWater.ID);
        }
        for(int i = 0; i < Math.ceil(tileAm*bioRepr); i++) {
            tilePool.add(TileBio.ID);
        }
        for(int i = 0; i < Math.ceil(tileAm*metalRepr); i++) {
            tilePool.add(TileMetal.ID);
        }

        tileAm += extraTileAm;
        for(int i = 0; i < extraTileAm; i++){
            tilePool.add(extraTileID);
        }

        tiles[tx][ty] = new TileEmpty(this, tx, ty);
        Station pod = new TravelPod(tiles[tx][ty]);
        this.stations.add(pod);
        tiles[tx][ty].addStation(pod);

        Config.debug("tiles: "+tileAm+" in pool: "+tilePool.size());
        while(tileAm > 0){
            int yoff = r.nextInt(3)-1;
            int xoff = r.nextInt(3)-1;
            //Config.debug("x "+(tx+xoff)+" y "+(ty+yoff));
            // + or - or no move in that direction
            if(tx+xoff < MAX_TILES_SIZE && tx+xoff >= 0 &&
                    ty+yoff < MAX_TILES_SIZE && ty+yoff >= 0){
                if(tiles[tx+xoff][ty+yoff] == null) {
                    int ti = r.nextInt(tilePool.size());
                    int t = tilePool.get(ti);
                    tilePool.remove(ti);
                    tiles[tx+xoff][ty+yoff] = getTileById(t, this,tx+xoff, ty+yoff);

                    /*if(tileAm == 4){
                        Station drill = new Greenhouse(tiles[tx][ty]);
                        this.stations.add(drill);
                        tiles[tx][ty].addStation(drill);
                    }
                    if(tileAm == 2){
                        Station drill = new O2Generator(tiles[tx][ty]);
                        this.stations.add(drill);
                        tiles[tx][ty].addStation(drill);
                    }*/

                    tileAm--;
                }else{
                    tx += xoff;
                    ty += yoff;
                }
            }
        }
    }

    private Tile getTileById(int id, Planet p, int tx, int ty){
        //return new TileMetal(p, tx, ty);
        switch (id){
            case TileEmpty.ID: return new TileEmpty(p, tx, ty);
            case TileBio.ID: return new TileBio(p, tx, ty);
            case TileMetal.ID: return new TileMetal(p, tx, ty);
            case TileWater.ID: return new TileWater(p, tx, ty);
        }
        Config.debug("ERROR! Missing tile by id: "+id);
        return new TileEmpty(p, tx, ty);
    }

    private Tile getRandomTile(Planet p, int tx, int ty){
        return getTileById(r.nextInt(4), p, tx, ty);
    }

    @Override
    public void render(Graphics g) {
        if(Game.viewState == Game.ViewState.SOLAR){
            if(img != null){
                g.setColor(Color.GRAY);
                g.drawOval(Config.X_MID-distance,
                        Config.Y_MID-distance,
                        distance*2, distance*2);
                g.drawImage(img, x, y, w, h, null);
            }
        }
        if(Game.viewState == Game.ViewState.PLANET){
            for(int r = 0; r < tiles.length; r++){
                for(int c = 0; c < tiles[0].length; c++){
                    if(tiles[c][r] != null)
                        tiles[c][r].render(g);
                }
            }
            for(Station s: stations)
                s.render(g);
        }

    }

    @Override
    public void update() {
        int speedMult = Game.simulation.speedup ? Game.simulation.speedupAm : 1;
        angle += 0.02*speed*speedMult;
        x = (int)((Config.X_MID-img.getWidth()*multiplier/2)+distance*Math.cos(Math.toRadians(angle)));
        y = (int)((Config.Y_MID-img.getWidth()*multiplier/2)+distance*Math.sin(Math.toRadians(angle)));
        this.w = img.getWidth()*multiplier;
        this.h = img.getHeight()*multiplier;
        // DO ONLY SOLAR RELATED UPDATES IN HERE
    }

    public static String getRandomName(){
        int i = new Random().nextInt(avaliableNames.size());
        String name = avaliableNames.get(i);
        avaliableNames.remove(i);
        return name;
    }

    public boolean isColonized() {
        return colonized;
    }

    public void setColonized(boolean b) {
        this.colonized = b;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public HashMap<String, String> getTileFiles() {
        return tileFiles;
    }

    public Tile[][] getTiles() {
        return tiles;
    }

    public ArrayList<Station> getStations() {
        return stations;
    }

    public String[] getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public BufferedImage getImg() {
        return img;
    }

    public float getUraniumChance() {
        return uraniumChance;
    }

    public float getPeopleChance() {
        return peopleChance;
    }

    public float getMetalChance() {
        return metalChance;
    }

    public float getDeathChance() {
        return deathChance;
    }
}
