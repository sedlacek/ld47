package com.sedlacek.ld47.world;

import com.sedlacek.ld47.graphics.ImageLoader;
import com.sedlacek.ld47.gui.Button;
import com.sedlacek.ld47.main.Config;
import com.sedlacek.ld47.main.Game;

import java.awt.*;

public class TravelPod extends Station{

    public static int GAS_NEEDED = 420;

    public TravelPod(Tile tile){
        this.name = "TravelPod \"Bullet\"  ";
        this.description = new String[]{
                "Used to enter a",
                "planet from orbit",
                "and to leave",
                "planet's orbit."
        };
        try {
            actionButton = new Button("Depart (-"+GAS_NEEDED+" gas)", 0, 0, 170, 25, Config.yellow.darker().darker(),
                    Color.BLACK, this.getClass().getDeclaredMethod("action"), this);
            actionButton.disableOnPause = true;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        this.image = ImageLoader.loadNS("/travelPod.png");
        this.tile = tile;
        this.w = 16*MULTIPLIER;
        this.h = 16*MULTIPLIER;
        this.x = tile.getX()+tile.getW()/2-w/2+2*MULTIPLIER;
        this.y = tile.getY()+tile.getH()/2-w/2-30;
        this.active = true;
    }

    @Override
    public void update() {
        super.update();
        if (Game.simulation.gas >= GAS_NEEDED) {
            actionButton.disabled = false;
        } else {
            actionButton.disabled = true;
        }
    }

    @Override
    public void action() {
        Game.simulation.gas -= GAS_NEEDED;
        Game.simulation.exitPlanet();
        Game.simulation.tutorial.exitPlanet();
        GAS_NEEDED += 100;
    }

    @Override
    public void clicked() {
        super.clicked();
        Game.simulation.tutorial.travelPod();
    }
}
