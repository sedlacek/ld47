package com.sedlacek.ld47.world;

import com.sedlacek.ld47.gui.Notification;
import com.sedlacek.ld47.main.Game;
import com.sedlacek.ld47.objects.Planet;
import com.sedlacek.ld47.player.Simulation;

import java.util.ArrayList;
import java.util.Random;

public class Exploration {

    private long timeSpent;
    private long timeNeeded;
    private Random r;
    private boolean inactive;
    private Planet p;
    private Simulation s;

    public static final int MIN_TIME_NEEDED = 40;
    public static final int MAX_TIME_NEEDED = 85;

    public static final int MAX_URANIUM = 4;
    public static final int MIN_URANIUM = 1;

    public static final int MAX_PEOPLE = 4;
    public static final int MIN_PEOPLE = 1;

    public static final int MAX_METAL = 80;
    public static final int MIN_METAL = 25;

    public static final int MAX_DEATH = 2;
    public static final int MIN_DEATH = 1;

    public Exploration(Planet p, Simulation s){
        this.s = s;
        this.p = p;
        r = new Random();
        this.timeNeeded = r.nextInt(MAX_TIME_NEEDED-MIN_TIME_NEEDED)+MIN_TIME_NEEDED;
        this.inactive = false;
    }

    public void fixedUpdate(){
        timeSpent++;
        if(!this.inactive && timeSpent >= timeNeeded){
            this.inactive = true;
            finished();
        }
    }

    public void finished(){
        int u = 0;
        int metal = 0;
        int people = 0;
        int death = 0;
        if(r.nextFloat() <= p.getUraniumChance()){
            // Drop U
            u=r.nextInt(MAX_URANIUM-MIN_URANIUM)+MIN_URANIUM;
        }
        if(r.nextFloat() <= p.getMetalChance()){
            metal = r.nextInt(MAX_METAL-MIN_METAL)+MIN_METAL;
        }
        float extrappl = Game.simulation.colonizedIndex == 0 ? 0.2f : 0f; //Boost chance for 1st planet
        if(r.nextFloat() <= p.getPeopleChance()+0.10+extrappl){
            people = r.nextInt(MAX_PEOPLE-MIN_PEOPLE)+MIN_PEOPLE;
        }
        float extraDeathChance = 0;
        if(s.temperature < -80 || s.temperature > 100){
            extraDeathChance += 0.3;
        }
        if(s.temperature < -150 || s.temperature > 200){
            extraDeathChance += 0.3;
        }
        if(s.temperature < 200 || s.temperature > 300){
            extraDeathChance += 0.3;
        }
        if(r.nextFloat() <= p.getDeathChance()+extraDeathChance){
            death = r.nextInt(MAX_DEATH-MIN_DEATH)+MIN_DEATH;
        }
        String title = "Scavengers have returned!";
        ArrayList<String> text = new ArrayList<>();
        if(death > 1){
            text.add("Unfortunately "+death+" men were lost.");
        }else if(death == 1){
            text.add("Unfortunately "+death+" man was lost.");
        }
        if(u == 0 && metal == 0 && people == 0){
            text.add("Nothing was found.");
        }else{
            text.add("We found:");
            if(u > 0){
                text.add(""+u+" uranium.");
            }
            if(metal > 0){
                text.add(""+metal+" metal.");
            }
            if(people > 0){
                text.add(""+people+" survivors.");
            }
        }
        Game.simulation.uranium += u;
        Game.simulation.metal += metal;
        Game.simulation.people += people - death;
        String[] desc = new String[text.size()];
        desc = text.toArray(desc);
        Game.simulation.addNotification(new Notification(title, desc, "OK"));
        s.stopExploration();
    }
}
