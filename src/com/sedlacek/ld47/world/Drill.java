package com.sedlacek.ld47.world;

import com.sedlacek.ld47.graphics.ImageLoader;
import com.sedlacek.ld47.gui.Button;
import com.sedlacek.ld47.main.Config;
import com.sedlacek.ld47.main.Game;

import java.awt.*;

public class Drill extends Station{

    public static final int PRICE = 15*6;

    public Drill(Tile tile){
        this.name = "Natural gas drill";
        this.description = new String[]{
                "Used to extract",
                "fuel from the",
                "planet's natural",
                "gas reservoirs."
        };
        try {
            actionButton = new Button("Assign", 0, 0, 130, 25, Config.yellow.darker().darker(),
                    Color.BLACK, this.getClass().getDeclaredMethod("action"), this);
            actionButton.disableOnPause = true;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        this.image = ImageLoader.loadNS("/drill2.png");
        this.tile = tile;
        this.w = 13*MULTIPLIER;
        this.h = 18*MULTIPLIER;
        this.x = tile.getX()+tile.getW()/2-w/2;
        this.y = tile.getY()+tile.getH()/2-h+3*MULTIPLIER;
        this.price = PRICE;
        this.gain = 1;
        this.crewSize = 8;
    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public void fixedUpdate(){
        if(this.active) {
            Game.simulation.gas += gain;
        }
    }

    @Override
    public void action() {
        super.action();
    }
}
