package com.sedlacek.ld47.objects;

import com.sedlacek.ld47.graphics.ImageLoader;
import com.sedlacek.ld47.gui.SolarOverlay;
import com.sedlacek.ld47.main.Config;
import com.sedlacek.ld47.main.Game;
import com.sedlacek.ld47.player.Simulation;
import com.sedlacek.ld47.player.SolarView;

import java.awt.*;
import java.util.Random;

public class ShipParis extends Ship{

    private Random r;

    public ShipParis(Simulation sim){
        this.solarIcon = ImageLoader.loadNS("/ship.png");
        this.planetIndex = 0;

        r = new Random();
        this.angle = r.nextInt(360);
        this.speed = r.nextFloat()+0.5f;
        this.distance = 16*3/2+10;

        this.sim = sim;
        this.h = solarIcon.getHeight()*multiplier;
        this.w = solarIcon.getWidth()*multiplier;
    }

    @Override
    public void update() {
        int speedMult = Game.simulation.speedup ? Game.simulation.speedupAm : 1;
        angle += 0.5*speed*speedMult;

        this.xc = sim.planets[planetIndex].getX()+sim.planets[planetIndex].getW()/2-w/2; //Centered
        this.x = (int)(xc+distance*Math.cos(Math.toRadians(angle)));
        this.yc = sim.planets[planetIndex].getY()+sim.planets[planetIndex].getH()/2-h/2;
        this.y = (int)(yc+distance*Math.sin(Math.toRadians(angle)));
    }

    @Override
    public void render(Graphics g) {
        g.setColor(new Color(0xD063FF));
        g.drawOval(xc-distance+w/2,
                yc-distance+w/2,
                distance*2, distance*2);
        g.drawImage(solarIcon, x, y, solarIcon.getWidth()*multiplier,
                solarIcon.getHeight()*multiplier, null);
    }
}
