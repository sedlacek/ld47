package com.sedlacek.ld47.player;

import com.sedlacek.ld47.gui.SolarOverlay;
import com.sedlacek.ld47.main.Config;
import com.sedlacek.ld47.objects.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

public class SolarView extends View{

    private Random r;

    private static final int BCK_STARS_MIN = 30;
    private static final int BCK_STARS_MAX = 55;
    private static final int BCK_STAR_SIZE_MIN = 2;
    private static final int BCK_STAR_SIZE_MAX = 7;

    private BufferedImage background;
    private Sun sun;
    private Ship ship;
    private Planet[] planets;

    public SolarOverlay overlay;
    private Simulation sim;

    public SolarView(Simulation sim, Sun sun, Planet[] planets, Ship ship){
        this.sim = sim;
        this.r = new Random();
        this.background = generateSpace();
        this.sun = sun;
        this.planets = planets;
        overlay = new SolarOverlay(sim);
        this.ship = ship;
    }

    @Override
    public void update(){
        super.update();
        overlay.update();
    }

    @Override
    public void render(Graphics g){
        super.render(g);
        g.drawImage(background, 0, 0, null);
        sun.render(g);

        for(Planet p: planets){
            if(p != null)
                p.render(g);
        }

        this.ship.render(g);
        overlay.render(g);
    }

    private BufferedImage generateSpace(){
        BufferedImage img = new BufferedImage(Config.WIDTH, Config.HEIGHT, BufferedImage.TYPE_INT_ARGB);
        Graphics g = img.getGraphics();

        g.setColor(Color.BLACK);
        g.fillRect(0,0, Config.WIDTH, Config.HEIGHT);

        int stars = r.nextInt(BCK_STARS_MAX-BCK_STARS_MIN)+BCK_STARS_MIN;
        for(int i = 0; i < stars; i++){
            int w = r.nextInt(BCK_STAR_SIZE_MAX-BCK_STAR_SIZE_MIN)+BCK_STAR_SIZE_MIN;
            // Different color for every star
            int roff = 255 - r.nextInt(25);
            int boff = 255 - r.nextInt(25);
            g.setColor(new Color(roff, 255, boff));
            int x = r.nextInt(Config.WIDTH-10)+5;
            int y = r.nextInt(Config.HEIGHT-10)+5;
            //Config.debug("x = "+x, " y = "+y);
            g.fillRect(x, y, w, w);
        }

        return img;
    }
}
