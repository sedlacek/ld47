package com.sedlacek.ld47.player;

import com.sedlacek.ld47.gui.MainOverlay;
import com.sedlacek.ld47.gui.Notification;
import com.sedlacek.ld47.gui.Tutorial;
import com.sedlacek.ld47.main.AudioGalery;
import com.sedlacek.ld47.main.Config;
import com.sedlacek.ld47.main.Game;
import com.sedlacek.ld47.main.KeyManager;
import com.sedlacek.ld47.objects.*;
import com.sedlacek.ld47.world.Exploration;
import com.sedlacek.ld47.world.Station;
import com.sedlacek.ld47.world.Tile;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Simulation {

    private Game game;
    public int colonizedIndex = -1;

    public SolarView solarView;
    public PlanetView planetView;
    public MainOverlay mainOverlay;

    private Sun sun;
    public Ship ship;
    public static final int[] planetDistances = new int[]{
            100, 200, 300, 400
    };
    public Planet[] planets;

    private Random r;
    private long fixedTicks = 0;

    private static final int MIN_STARVATION_DEATH = 1;
    private static final int MAX_STARVATION_DEATH = 3;
    private static final int RND_FOOD_MAX = 8;
    private static final int RND_FOOD_MIN = -3;
    private static final int RND_O2_MAX = 4;
    private static final int RND_O2_MIN = -2;

    public static final int URANIUM_NEEDED = 80;

    private static boolean highHeatTip = false;
    private static boolean freezingTip = false;

    public int temperature;

    public int metal;
    public int o2;
    public int food;
    public int gas;
    public int people;
    public int working;
    public int uranium;
    public int explorationSize=5;
    public int exploring=0;

    public boolean speedup = false;
    public int speedupAm = 4;

    private Exploration exploration;
    public Tutorial tutorial;

    public Simulation(Game game){
        this.game = game;
        sun = new Sun();
        r = new Random();
        this.planets = new Planet[4];

        generateRandomPlanets();

        this.people = 25;
        this.o2 = 1500;
        this.metal = 20*6;
        this.food = 100;
        this.gas = 0;
        this.uranium = 0;

        this.ship = new ShipParis(this);

        this.mainOverlay = new MainOverlay(this);
        this.solarView = new SolarView(this, sun, planets, ship);
        this.planetView = new PlanetView(this);

        //colonize(0);
        tutorial = new Tutorial(this);
    }

    public void update(){
        if(Game.viewState == Game.ViewState.PLANET){
            planetView.update();
        }else{ // Solar view
            solarView.update();
        }
        this.mainOverlay.update();

        if(getColonizedPlanet() != null) {
            Planet p = getColonizedPlanet();
            Tile[][] tiles = p.getTiles();

            for (int r = 0; r < tiles.length; r++) {
                for (int c = 0; c < tiles[0].length; c++) {
                    if (tiles[c][r] != null)
                        tiles[c][r].update();
                }
            }
        }

        if(Game.paused && speedup){
            speedUpTime(); //Turn off speedup on pause
        }

        if(!Game.paused) {
            sun.update();
            ship.update();

            if(Game.getKeyManager().keys[KeyManager.R]){
                speedUpTime();
                Game.getKeyManager().keys[KeyManager.R] = false;
            }

            for (Planet p : planets) {
                if(p != null)
                    p.update(); // Only for rotaion
            }

            if(getColonizedPlanet() != null){
                Planet p = getColonizedPlanet();

                for(Station s: p.getStations()){
                    s.update();
                }
            }
        }

        this.temperature = 400-((planetDistances[ship.getPlanetIndex()]-sun.radius)*5);
        if(temperature < 0){ //  Different ratio for negative values
            this.temperature = (int)-((planetDistances[ship.getPlanetIndex()]-sun.radius)*1.5f);
        }
        //Config.debug("T: "+this.temperature);
        if(temperature >= 400 || temperature <= -400){
            Game.gameOver(temperature <= -400);
        }
        else if(temperature > 300 && !highHeatTip){
            addNotification(new Notification("Watchout for the teperature",
                    new String[]{"When the temperature reaches", "above 400 C or bellow -400 C", "you won't survive"},
                    "OK"));
            highHeatTip = true;
        }
        else if(temperature < -250 && !freezingTip){
            addNotification(new Notification("Watchout for the teperature",
                    new String[]{"When the temperature reaches", "bellow -400 C or above 400 C", "you won't survive"},
                    "OK..."));
            freezingTip = true;
        }
    }

    public void addNotification(Notification n){
        this.mainOverlay.notifications.add(n);
    }

    public void popNotification(){
        if(this.mainOverlay.notifications.size() > 0) {
            this.mainOverlay.notifications.remove(0);
            Game.paused = false;
        }
    }

    public void fixedUpdate(){
        if(!Game.paused) {
            if(getColonizedPlanet() != null){
                Planet p = getColonizedPlanet();

                for(Station s: p.getStations()){
                    s.fixedUpdate();
                }
            }

            if(exploration != null){
                exploration.fixedUpdate();
            }

            fixedTicks++;
            if(fixedTicks >= 60) {
                // Use food
                int extra = people > RND_FOOD_MIN*-1 ? r.nextInt(RND_FOOD_MAX-RND_FOOD_MIN)+RND_FOOD_MIN : 0;
                food -= people+extra; // 1 per person per minute
                if(food < 0){
                    int dying = r.nextInt(MAX_STARVATION_DEATH-MIN_STARVATION_DEATH)+MIN_STARVATION_DEATH;
                    if(dying > people){
                        dying = people;
                    }
                    addNotification(new Notification("Starvation!", new String[]{"People ("+dying+") have died",
                            "of starvation. Get more", "food for your people!"}, "Oh no!"));
                    people-= dying;
                    food = 0;
                }
                fixedTicks-=60;
                // Gain passive iron (to avoid being stuck)
                metal+=r.nextInt(3);
            }
            // Use up oxygen 1 per person per second
            int extraO2 = people > RND_O2_MIN*-1 ? r.nextInt(RND_O2_MAX-RND_O2_MIN)+RND_O2_MIN : 0;
            o2-=people+extraO2;

            // Kill off people if there is not enough oxygen
            if(o2 < 0){
                people += o2;
                o2 = 0;
            }

            if(people <= 0){
                Game.gameOver("Your colony has died out...");
            }
        }
    }

    public void render(Graphics g){
        if(Game.viewState == Game.ViewState.PLANET){
            planetView.render(g);
        }else{ // Solar view
            solarView.render(g);
        }
        this.mainOverlay.render(g);
    }

    public void speedUpTime(){
        if(!speedup) {
            Sun.expansionInterval = Sun.EXPANSION_INTERVAL_FIXED / speedupAm;
        }else{
            Sun.expansionInterval = Sun.EXPANSION_INTERVAL_FIXED;
        }
        this.speedup = !this.speedup;
    }

    public void colonize(int index) {
        if(!planets[index].isColonized()){ // Colonize
            planets[index].setColonized(true);
            colonizedIndex = index;
            Game.viewState = Game.ViewState.PLANET;
            tutorial.colonized();
        }else{ // Abandon
            Game.viewState = Game.ViewState.SOLAR;
        }
    }

    public void exitPlanet(){
        colonizedIndex = -1;
        Game.viewState = Game.ViewState.SOLAR;
        planetView.overlay.closeInfo();
        working = 0;
        exploring = 0;
    }

    public void startExploration(){
        if(getColonizedPlanet() == null) {
            System.err.println("Error: Exploration started without colonization");
            return;
        }
        this.exploring = this.explorationSize;
        this.exploration = new Exploration(getColonizedPlanet(), this);
        this.working += this.explorationSize;
    }

    public void stopExploration(){
        this.exploring = 0;
        this.exploration = null;
        this.working -= this.explorationSize;
    }

    public void exitOrbit(int index) {
        if(index == 3){
            Game.gameWon();
        }else {
            solarView.overlay.closeInfo();
            ship.setPlanetIndex(index + 1);
            colonizedIndex = -1;
            //solarView.overlay.bPlanets[index+1].active = true;
            //solarView.overlay.planetClicked(index+1);
        }
    }

    public void destroyPlanet(int index){
        Config.debug("Destroyed "+index);
        Notification n = new Notification("The sun has devoured planet "+this.planets[index].getName()+"!",
                new String[]{"The sun is rapidly expanding and", "destroying everything in it's way."}, "...");
        n.setSound(AudioGalery.badNotification);
        addNotification(n);
        if(ship.getPlanetIndex() == index){
            Game.gameOver(false);
        }
        else {
            this.planets[index] = null;
        }
    }

    private Planet getRandomPlanet(int distance){
        return getPlanet(r.nextInt(5), distance);
    }

    private Planet getPlanet(int i, int distance){
        switch (i){
            case 0:
                return new GreenPlanet(distance);
            case 1:
                return new DesertPlanet(distance);
            case 2:
                return new JunglePlanet(distance);
            case 3:
                return new WaterPlanet(distance);
            case 4:
                return new LavaPlanet(distance);
        }
        return new GreenPlanet(distance);
    }

    private void generateRandomPlanets(){
        ArrayList<Integer> genPlanets = new ArrayList<>(Arrays.asList(0,1,2,3,4));

        for(int a = 0; a < planetDistances.length; a++) {
            int ri = r.nextInt(genPlanets.size());
            int id = genPlanets.get(ri);
            genPlanets.remove(ri);
            this.planets[a] = getPlanet(id, planetDistances[a]);
        }
    }

    public Planet getColonizedPlanet(){
        if(colonizedIndex > -1)
            return planets[colonizedIndex];
        return null;
    }
}
