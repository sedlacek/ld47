package com.sedlacek.ld47.world;

import com.sedlacek.ld47.graphics.ImageLoader;
import com.sedlacek.ld47.gui.GUI;
import com.sedlacek.ld47.main.Config;
import com.sedlacek.ld47.main.Game;
import com.sedlacek.ld47.main.GameObject;
import com.sedlacek.ld47.objects.Planet;

import java.awt.*;
import java.awt.image.BufferedImage;

public abstract class Tile extends GUI {

    protected BufferedImage img;
    protected int col, row;
    protected Planet p;
    protected Station station;
    protected int id;
    protected boolean buildable;

    public static final int MULTIPLIER = 7;
    public static final BufferedImage highlightImg = ImageLoader.loadNS("/tileHighlight.png");
    public static final BufferedImage highlightCorrectImg = ImageLoader.loadNS("/tileHighlightCorrect.png");
    public static final BufferedImage highlightIncorrectImg = ImageLoader.loadNS("/tileHighlightIncorrect.png");

    public Tile(Planet p, int col, int row){
        this.p = p;

        this.col = col;
        this.row = row;
        this.w = 16*MULTIPLIER;
        this.h = 12*MULTIPLIER;

        this.x = Config.WIDTH/2-Planet.MAX_TILES_SIZE/2*w+col*w;
        if(row % 2 == 0)
            x += 8*MULTIPLIER;
        this.y = Config.HEIGHT/2-Planet.MAX_TILES_SIZE/2*h+row*h;
    }

    public void addStation(Station s){
        this.station = s;
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(img, x, y, w, 16*MULTIPLIER, null);
        if(mouseOver){
            g.drawImage(highlightImg, x, y, w, 16*MULTIPLIER, null);
        }
        if(this.buildable && this.station == null){
            g.drawImage(highlightCorrectImg, x, y, w, 16*MULTIPLIER, null);
        }
        if(this.station != null && !station.active){
            g.drawImage(highlightIncorrectImg, x, y, w, 16*MULTIPLIER, null);
        }
    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public void clicked() {
        if(station != null) {
            station.clicked();
        }
        if(Game.simulation.planetView.builderGUI.stationBuilder != null){
            Game.simulation.planetView.builderGUI.build(this);
        }else {
            Game.simulation.planetView.overlay.stationClicked(station);
        }
    }

    public boolean isBuildable() {
        return buildable;
    }

    public Station getStation() {
        return station;
    }
}
