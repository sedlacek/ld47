package com.sedlacek.ld47.objects;

import com.sedlacek.ld47.graphics.ImageLoader;

public class GreenPlanet extends Planet {

    public GreenPlanet(int distance){
        super(distance);
        this.img = ImageLoader.loadNS("/planet1.png");
        this.id = 0;
        this.description = new String[]{
                "Green planet with",
                "a lots of life",
                "forms."
        };
        this.bioRepr = this.metalRepr = this.waterRepr = this.emptyRepr = 0.25f;
        super.generateMap();
        this.uraniumChance = 0.4f;
        this.metalChance = 0.4f;
        this.peopleChance = 0.58f;
        this.deathChance = 0.1f;
    }
}
