package com.sedlacek.ld47.gui;

import com.sedlacek.ld47.main.Config;
import com.sedlacek.ld47.main.Game;
import com.sedlacek.ld47.main.KeyManager;
import com.sedlacek.ld47.player.Simulation;

import java.awt.*;

public class SolarOverlay {

    public static PlanetButton[] bPlanets;
    private Simulation sim;
    private PlanetInfo planetInfo;
    private int activePlanet = -1;

    public SolarOverlay(Simulation sim){
        this.sim = sim;
        try {
            bPlanets = new PlanetButton[]{
                    new PlanetButton(sim.planets[0], SolarOverlay.class.getDeclaredMethod("planet1Clicked"),
                            this),
                    new PlanetButton(sim.planets[1], SolarOverlay.class.getDeclaredMethod("planet2Clicked"),
                            this),
                    new PlanetButton(sim.planets[2], SolarOverlay.class.getDeclaredMethod("planet3Clicked"),
                            this),
                    new PlanetButton(sim.planets[3], SolarOverlay.class.getDeclaredMethod("planet4Clicked"),
                            this),
            };
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        planetInfo = null;
    }

    public void update(){
        for(PlanetButton p: bPlanets)
            p.update();
        if(planetInfo != null)
            planetInfo.update();
        if(Game.getKeyManager().keys[KeyManager.ESC] && planetInfo != null){
            closeInfo();
        }
    }

    public void render(Graphics g){
        if(planetInfo != null)
            planetInfo.render(g);
    }

    public void closeInfo(){
        bPlanets[activePlanet].active = false;
        activePlanet = -1;
        planetInfo = null;
    }

    public void planetClicked(int index){
        if(bPlanets[index].active) {
            this.activePlanet = index;
            this.planetInfo = new PlanetInfo(sim, sim.planets[index], index);
        }else {
            closeInfo();
        }
    }

    public void planet1Clicked(){
        Config.debug("Clicked 1");
        planetClicked(0);
    }

    public void planet2Clicked(){
        Config.debug("Clicked 2");
        planetClicked(1);
    }

    public void planet3Clicked(){
        Config.debug("Clicked 3");
        planetClicked(2);
    }

    public void planet4Clicked(){
        Config.debug("Clicked 4");
        planetClicked(3);
    }
}
