package com.sedlacek.ld47.gui;

import com.sedlacek.ld47.main.AudioGalery;
import com.sedlacek.ld47.main.AudioPlayer;
import com.sedlacek.ld47.main.Config;
import com.sedlacek.ld47.main.Game;

import java.awt.*;

public class Notification extends GUI{

    public String text[], buttonText;
    public Button button;
    public String title;
    private String longest;

    private int xoffs = 10;
    private int yoffs = 30;
    private int space = 30;

    public Color backColor = Config.yellow;
    public Color textColor = new Color(0xEAEAEA);
    public Color titleColor = Color.WHITE;
    public Font titleFont = new Font("DorFont03", Font.BOLD, 32);
    public Font font = new Font("DorFont03", Font.PLAIN, 24);

    public AudioPlayer sound = AudioGalery.goodNotification;

    boolean showed = false;

    public Notification(String title, String[] text, String buttonText){
        this.title = title;
        this.text = text;
        this.buttonText = buttonText;
        this.w = 200;
        this.h = text.length*space+space*2+25;
        this.x = Config.WIDTH/2-w/2;
        this.y = Config.HEIGHT/2-h/2;
        longest = title;
        for(String s: text){
            if(s.length() > longest.length()){
                longest = s;
            }
        }
        try {
            this.button = new Button(buttonText, x+space, y+h-25-5, 90,25,
                    Config.yellow.darker().darker(), Color.BLACK,
                    this.getClass().getDeclaredMethod("clicked"), this);
            this.button.disableOnPause = false;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update() {
        super.update();
        if(!showed){
            sound.playClip();
            this.showed = true;
        }
        button.update();
        if(Game.simulation.mainOverlay.notifications.size() > 0)
            Game.paused = true;
    }

    @Override
    public void render(Graphics g) {
        g.setFont(titleFont);
        this.w = g.getFontMetrics().stringWidth(longest)+xoffs*2;
        this.button.setW(w-space*2);
        this.x = Config.WIDTH/2-w/2;
        this.button.setX(x+space);
        g.setColor(backColor);
        g.fillRoundRect(x, y, w, h, 20, 20);

        g.setColor(titleColor);
        g.drawString(title, x+w/2-g.getFontMetrics().stringWidth(title)/2, y+yoffs);

        g.setColor(textColor);
        g.setFont(font);
        for(int i = 0; i < text.length; i++){
            g.drawString(text[i], x+w/2-g.getFontMetrics().stringWidth(text[i])/2, y+yoffs+space*i+space+10);
        }

        button.render(g);
    }

    @Override
    public void clicked() {
        Game.simulation.popNotification();
    }

    public void setSound(AudioPlayer sound) {
        this.sound = sound;
    }
}
